Utilities for controlling whether an element should explicitly create a new stacking context.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>isolate</td><td>isolation: isolate;</td></tr>
    <tr><td>isolation-auto</td><td>isolation: auto;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/isolation)
