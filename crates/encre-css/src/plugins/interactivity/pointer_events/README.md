Utilities for controlling whether an element responds to pointer events.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>pointer-events-none</td><td>pointer-events: none;</td></tr>
    <tr><td>pointer-events-auto</td><td>pointer-events: auto;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/pointer-events)
