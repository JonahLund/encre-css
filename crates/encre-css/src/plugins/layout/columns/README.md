Utilities for controlling the number of columns within an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>columns-<i>&lt;integer&gt;</i></td><td>columns: <i>&lt;integer&gt;</i>;</td></tr>
    <tr><td>columns-auto</td><td>columns: auto;</td></tr>
    <tr><td>columns-3xs</td><td>columns: 16rem;</td></tr>
    <tr><td>columns-2xs</td><td>columns: 18rem;</td></tr>
    <tr><td>columns-xs</td><td>columns: 20rem;</td></tr>
    <tr><td>columns-sm</td><td>columns: 24rem;</td></tr>
    <tr><td>columns-md</td><td>columns: 28rem;</td></tr>
    <tr><td>columns-lg</td><td>columns: 32rem;</td></tr>
    <tr><td>columns-xl</td><td>columns: 36rem;</td></tr>
    <tr><td>columns-2xl</td><td>columns: 42rem;</td></tr>
    <tr><td>columns-3xl</td><td>columns: 48rem;</td></tr>
    <tr><td>columns-4xl</td><td>columns: 56rem;</td></tr>
    <tr><td>columns-5xl</td><td>columns: 64rem;</td></tr>
    <tr><td>columns-6xl</td><td>columns: 72rem;</td></tr>
    <tr><td>columns-7xl</td><td>columns: 80rem;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/columns)
