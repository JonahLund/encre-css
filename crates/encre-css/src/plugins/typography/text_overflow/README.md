Utilities for controlling text overflow in an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>truncate</td><td>overflow: hidden;<br>text-overflow: ellipsis;<br>white-space: nowrap;</td></tr>
    <tr><td>text-ellipsis</td><td>text-overflow: ellipsis;</td></tr>
    <tr><td>text-clip</td><td>text-overflow: clip;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/text-overflow)
