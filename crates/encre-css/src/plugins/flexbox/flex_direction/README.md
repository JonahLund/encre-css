Utilities for controlling the direction of flex items.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>flex-row</td><td>flex-direction: row;</td></tr>
    <tr><td>flex-row-reverse</td><td>flex-direction: row-reverse;</td></tr>
    <tr><td>flex-col</td><td>flex-direction: column;</td></tr>
    <tr><td>flex-col-reverse</td><td>flex-direction: column-reverse;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/flex-direction)
