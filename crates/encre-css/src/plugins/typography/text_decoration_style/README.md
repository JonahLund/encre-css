Utilities for controlling the style of text decorations.

<style>
#text-decoration-style-table tr td:nth-child(3) p {
  text-decoration: underline;
  text-decoration-thickness: 2px;
  text-underline-offset: 2px;
}
</style>

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
      <th style="text-align: center; width: 10rem;">Preview</th>
    </tr>
  </thead>
  <tbody id="text-decoration-style-table">
    <tr><td>decoration-solid</td><td>text-decoration-style: solid;</td><td><p style="text-decoration-style: solid;">Lorem ipsum</p></td></tr>
    <tr><td>decoration-double</td><td>text-decoration-style: double;</td><td><p style="text-decoration-style: double;">Lorem ipsum</p></td></tr>
    <tr><td>decoration-dotted</td><td>text-decoration-style: dotted;</td><td><p style="text-decoration-style: dotted;">Lorem ipsum</p></td></tr>
    <tr><td>decoration-dashed</td><td>text-decoration-style: dashed;</td><td><p style="text-decoration-style: dashed;">Lorem ipsum</p></td></tr>
    <tr><td>decoration-wavy</td><td>text-decoration-style: wavy;</td><td><p style="text-decoration-style: wavy;">Lorem ipsum</p></td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/text-decoration-style)
