Utilities for controlling how rows are positioned in multi-row flex and grid containers.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>content-center</td>
      <td>align-content: center;</td>
    </tr>
    <tr>
      <td>content-start</td>
      <td>align-content: flex-start;</td>
    </tr>
    <tr>
      <td>content-end</td>
      <td>align-content: flex-end;</td>
    </tr>
    <tr>
      <td>content-between</td>
      <td>align-content: space-between;</td>
    </tr>
    <tr>
      <td>content-around</td>
      <td>align-content: space-around;</td>
    </tr>
    <tr>
      <td>content-evenly</td>
      <td>align-content: space-evenly;</td>
    </tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/align-content)
