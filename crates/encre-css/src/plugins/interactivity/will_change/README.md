Utilities for optimizing upcoming animations of elements that are expected to change.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>will-change-auto</td><td>will-change: auto;</td></tr>
    <tr><td>will-change-scroll</td><td>will-change: scroll-position;</td></tr>
    <tr><td>will-change-contents</td><td>will-change: contents;</td></tr>
    <tr><td>will-change-transform</td><td>will-change: transform;</td></tr>
  </tbody>
</table>

### Arbitrary values

Any property is allowed as arbitrary value.
For example, `will-change-[opacity]`.

[Tailwind reference](https://tailwindcss.com/docs/will-change)
