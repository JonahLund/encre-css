Utilities for controlling the style of an element's outline.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>outline-none</td><td>outline: 2px solid transparent;<br>outline-offset: 2px;</td></tr>
    <tr><td>outline</td><td>outline-style: solid;</td></tr>
    <tr><td>outline-dashed</td><td>outline-style: dashed;</td></tr>
    <tr><td>outline-dotted</td><td>outline-style: dotted;</td></tr>
    <tr><td>outline-double</td><td>outline-style: double;</td></tr>
    <tr><td>outline-hidden</td><td>outline-style: hidden;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/outline-style)
