Utilities for controlling whether the user can select text in an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>select-none</td><td>user-select: none;</td></tr>
    <tr><td>select-text</td><td>user-select: text;</td></tr>
    <tr><td>select-all</td><td>user-select: all;</td></tr>
    <tr><td>select-auto</td><td>user-select: auto;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/user-select)
