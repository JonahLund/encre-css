Utilities for controlling how flex and grid items are positioned along a container's main axis.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>justify-start</td>
      <td>justify-content: flex-start;</td>
    </tr>
    <tr>
      <td>justify-end</td>
      <td>justify-content: flex-end;</td>
    </tr>
    <tr>
      <td>justify-center</td>
      <td>justify-content: center;</td>
    </tr>
    <tr>
      <td>justify-between</td>
      <td>justify-content: space-between;</td>
    </tr>
    <tr>
      <td>justify-around</td>
      <td>justify-content: space-around;</td>
    </tr>
    <tr>
      <td>justify-evenly</td>
      <td>justify-content: space-evenly;</td>
    </tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/justify-content)
