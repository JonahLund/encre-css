Utilities for controlling how the browser should calculate an element's total size.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>box-border</td><td>box-sizing: border-box;</td></tr>
    <tr><td>box-content</td><td>box-sizing: content-box;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/box-sizing)
