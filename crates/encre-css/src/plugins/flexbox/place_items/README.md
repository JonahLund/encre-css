Utilities for controlling how items are justified and aligned at the same time.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>place-items-start</td>
      <td>place-items: start;</td>
    </tr>
    <tr>
      <td>place-items-end</td>
      <td>place-items: end;</td>
    </tr>
    <tr>
      <td>place-items-center</td>
      <td>place-items: center;</td>
    </tr>
    <tr>
      <td>place-items-stretch</td>
      <td>place-items: stretch;</td>
    </tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/place-items)
