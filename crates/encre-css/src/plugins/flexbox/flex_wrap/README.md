Utilities for controlling how flex items wrap.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>flex-wrap</td><td>flex-wrap: wrap;</td></tr>
    <tr><td>flex-wrap-reverse</td><td>flex-wrap: wrap-reverse;</td></tr>
    <tr><td>flex-nowrap</td><td>flex-wrap: nowrap;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/flex-wrap)
