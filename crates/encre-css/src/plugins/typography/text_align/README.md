Utilities for controlling the alignment of text.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>text-left</td><td>text-align: left;</td></tr>
    <tr><td>text-center</td><td>text-align: center;</td></tr>
    <tr><td>text-right</td><td>text-align: right;</td></tr>
    <tr><td>text-justify</td><td>text-align: justify;</td></tr>
    <tr><td>text-start</td><td>text-align: start;</td></tr>
    <tr><td>text-end</td><td>text-align: end;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/text-align)
