Utilities for suppressing native form control styling.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>appearance-none</td><td>appearance: none;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/appearance)
