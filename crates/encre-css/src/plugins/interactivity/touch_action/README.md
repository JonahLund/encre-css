Utilities for controlling how an element can be scrolled and zoomed on touchscreens.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>touch-auto</td><td>touch-action: auto;</td></tr>
    <tr><td>touch-none</td><td>touch-action: none;</td></tr>
    <tr><td>touch-pan-x</td><td>touch-action: pan-x;</td></tr>
    <tr><td>touch-pan-left</td><td>touch-action: pan-left;</td></tr>
    <tr><td>touch-pan-right</td><td>touch-action: pan-right;</td></tr>
    <tr><td>touch-pan-y</td><td>touch-action: pan-y;</td></tr>
    <tr><td>touch-pan-up</td><td>touch-action: pan-up;</td></tr>
    <tr><td>touch-pan-down</td><td>touch-action: pan-down;</td></tr>
    <tr><td>touch-pinch-zoom</td><td>touch-action: pinch-zoom;</td></tr>
    <tr><td>touch-manipulation</td><td>touch-action: manipulation;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/touch-action)
