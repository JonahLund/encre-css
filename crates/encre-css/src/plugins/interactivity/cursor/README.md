Utilities for controlling the cursor style when hovering over an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr style="cursor: auto;"><td>cursor-auto</td><td>cursor: auto;</td></tr>
    <tr style="cursor: default;"><td>cursor-default</td><td>cursor: default;</td></tr>
    <tr style="cursor: pointer;"><td>cursor-pointer</td><td>cursor: pointer;</td></tr>
    <tr style="cursor: wait;"><td>cursor-wait</td><td>cursor: wait;</td></tr>
    <tr style="cursor: text;"><td>cursor-text</td><td>cursor: text;</td></tr>
    <tr style="cursor: move;"><td>cursor-move</td><td>cursor: move;</td></tr>
    <tr style="cursor: help;"><td>cursor-help</td><td>cursor: help;</td></tr>
    <tr style="cursor: not-allowed;"><td>cursor-not-allowed</td><td>cursor: not-allowed;</td></tr>
    <tr style="cursor: none;"><td>cursor-none</td><td>cursor: none;</td></tr>
    <tr style="cursor: context-menu;"><td>cursor-context-menu</td><td>cursor: context-menu;</td></tr>
    <tr style="cursor: progress;"><td>cursor-progress</td><td>cursor: progress;</td></tr>
    <tr style="cursor: cell;"><td>cursor-cell</td><td>cursor: cell;</td></tr>
    <tr style="cursor: crosshair;"><td>cursor-crosshair</td><td>cursor: crosshair;</td></tr>
    <tr style="cursor: vertical-text;"><td>cursor-vertical-text</td><td>cursor: vertical-text;</td></tr>
    <tr style="cursor: alias;"><td>cursor-alias</td><td>cursor: alias;</td></tr>
    <tr style="cursor: copy;"><td>cursor-copy</td><td>cursor: copy;</td></tr>
    <tr style="cursor: no-drop;"><td>cursor-no-drop</td><td>cursor: no-drop;</td></tr>
    <tr style="cursor: grab;"><td>cursor-grab</td><td>cursor: grab;</td></tr>
    <tr style="cursor: grabbing;"><td>cursor-grabbing</td><td>cursor: grabbing;</td></tr>
    <tr style="cursor: all-scroll;"><td>cursor-all-scroll</td><td>cursor: all-scroll;</td></tr>
    <tr style="cursor: col-resize;"><td>cursor-col-resize</td><td>cursor: col-resize;</td></tr>
    <tr style="cursor: row-resize;"><td>cursor-row-resize</td><td>cursor: row-resize;</td></tr>
    <tr style="cursor: n-resize;"><td>cursor-n-resize</td><td>cursor: n-resize;</td></tr>
    <tr style="cursor: e-resize;"><td>cursor-e-resize</td><td>cursor: e-resize;</td></tr>
    <tr style="cursor: s-resize;"><td>cursor-s-resize</td><td>cursor: s-resize;</td></tr>
    <tr style="cursor: w-resize;"><td>cursor-w-resize</td><td>cursor: w-resize;</td></tr>
    <tr style="cursor: ne-resize;"><td>cursor-ne-resize</td><td>cursor: ne-resize;</td></tr>
    <tr style="cursor: nw-resize;"><td>cursor-nw-resize</td><td>cursor: nw-resize;</td></tr>
    <tr style="cursor: se-resize;"><td>cursor-se-resize</td><td>cursor: se-resize;</td></tr>
    <tr style="cursor: sw-resize;"><td>cursor-sw-resize</td><td>cursor: sw-resize;</td></tr>
    <tr style="cursor: ew-resize;"><td>cursor-ew-resize</td><td>cursor: ew-resize;</td></tr>
    <tr style="cursor: ns-resize;"><td>cursor-ns-resize</td><td>cursor: ns-resize;</td></tr>
    <tr style="cursor: nesw-resize;"><td>cursor-nesw-resize</td><td>cursor: nesw-resize;</td></tr>
    <tr style="cursor: nwse-resize;"><td>cursor-nwse-resize</td><td>cursor: nwse-resize;</td></tr>
    <tr style="cursor: zoom-in;"><td>cursor-zoom-in</td><td>cursor: zoom-in;</td></tr>
    <tr style="cursor: zoom-out;"><td>cursor-zoom-out</td><td>cursor: zoom-out;</td></tr>
  </tbody>
</table>

### Arbitrary values

Any property is allowed as arbitrary value.
For example, `cursor-[url(/pointer.png)]`.

[Tailwind reference](https://tailwindcss.com/docs/cursor)
