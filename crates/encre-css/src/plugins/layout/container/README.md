A component for fixing an element's width to the current breakpoint.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Breakpoint</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td rowspan="6">container</td><td><i>None</i></td><td>width: 100%;</td></tr>
    <tr><td><i>sm (640px)</i></td><td>max-width: 640px;</td></tr>
    <tr><td><i>md (768px)</i></td><td>max-width: 768px;</td></tr>
    <tr><td><i>lg (1024px)</i></td><td>max-width: 1024px;</td></tr>
    <tr><td><i>xl (1280px)</i></td><td>max-width: 1280px;</td></tr>
    <tr><td><i>2xl (1536px)</i></td><td>max-width: 1536px;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/container)
