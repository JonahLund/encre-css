Utilities for controlling how a column or page should break before an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>break-before-auto</td><td>break-before: auto;</td></tr>
    <tr><td>break-before-avoid</td><td>break-before: avoid;</td></tr>
    <tr><td>break-before-all</td><td>break-before: all;</td></tr>
    <tr><td>break-before-avoid-page</td><td>break-before: avoid-page;</td></tr>
    <tr><td>break-before-page</td><td>break-before: page;</td></tr>
    <tr><td>break-before-left</td><td>break-before: left;</td></tr>
    <tr><td>break-before-right</td><td>break-before: right;</td></tr>
    <tr><td>break-before-column</td><td>break-before: column;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/break-before)
