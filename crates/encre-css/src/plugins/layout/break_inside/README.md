Utilities for controlling how a column or page should break within an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>break-inside-auto</td><td>break-inside: auto;</td></tr>
    <tr><td>break-inside-avoid</td><td>break-inside: avoid;</td></tr>
    <tr><td>break-inside-avoid-page</td><td>break-inside: avoid-page;</td></tr>
    <tr><td>break-inside-avoid-column</td><td>break-inside: avoid-column;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/break-inside)
