Utilities for controlling the border style between elements.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>divide-solid</td><td>border-style: solid;</td></tr>
    <tr><td>divide-dashed</td><td>border-style: dashed;</td></tr>
    <tr><td>divide-dotted</td><td>border-style: dotted;</td></tr>
    <tr><td>divide-double</td><td>border-style: double;</td></tr>
    <tr><td>divide-none</td><td>border-style: none;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/divide-style)
