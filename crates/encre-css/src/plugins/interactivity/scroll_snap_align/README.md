Utilities for controlling the scroll snap alignment of an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>snap-start</td><td>scroll-snap-align: start;</td></tr>
    <tr><td>snap-end</td><td>scroll-snap-align: end;</td></tr>
    <tr><td>snap-center</td><td>scroll-snap-align: center;</td></tr>
    <tr><td>snap-align-none</td><td>scroll-snap-align: none;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/scroll-snap-align)
