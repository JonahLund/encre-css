Utilities for controlling the position of bullets/numbers in lists.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>list-inside</td><td>list-style-position: inside;</td></tr>
    <tr><td>list-outside</td><td>list-style-position: outside;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/list-style-position)
