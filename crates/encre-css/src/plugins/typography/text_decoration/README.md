Utilities for controlling the decoration of text.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
      <th style="text-align: center; width: 10rem;">Preview</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>underline</td><td>text-decoration-line: underline;</td><td><p style="text-decoration-line: underline;">Lorem ipsum</p></td></tr>
    <tr><td>overline</td><td>text-decoration-line: overline;</td><td><p style="text-decoration-line: overline;">Lorem ipsum</p></td></tr>
    <tr><td>line-through</td><td>text-decoration-line: line-through;</td><td><p style="text-decoration-line: line-through;">Lorem ipsum</p></td></tr>
    <tr><td>no-underline</td><td>text-decoration-line: none;</td><td><p style="text-decoration-line: none;">Lorem ipsum</p></td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/text-decoration)
