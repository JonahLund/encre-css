Utilities for controlling the text color of an element.

<style>
#text-color-table > tr td:nth-child(3) {
  font-family: sans-serif;
  font-weight: 900;
  font-size: 1.2rem;
}
</style>

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
      <th style="text-align: center;">Color</th>
    </tr>
  </thead>
  <tbody id="text-color-table">
    <tr><td>text-inherit</td><td>color: inherit;</td><td style="color: inherit;">Lorem</td></tr>
    <tr><td>text-current</td><td>color: currentColor;</td><td style="color: currentColor;">Lorem</td></tr>
    <tr><td>text-transparent</td><td>color: transparent;</td><td style="color: transparent;">Lorem</td></tr>
    <tr><td>text-black</td><td>color: rgb(0 0 0);</td><td style="color: rgb(0 0 0);">Lorem</td></tr>
    <tr><td>text-white</td><td>color: rgb(255 255 255);</td><td style="color: rgb(255 255 255);">Lorem</td></tr>
    <tr><td>text-slate-50</td><td>color: rgb(248 250 252);</td><td style="color: rgb(248 250 252);">Lorem</td></tr>
    <tr><td>text-slate-100</td><td>color: rgb(241 245 249);</td><td style="color: rgb(241 245 249);">Lorem</td></tr>
    <tr><td>text-slate-200</td><td>color: rgb(226 232 240);</td><td style="color: rgb(226 232 240);">Lorem</td></tr>
    <tr><td>text-slate-300</td><td>color: rgb(203 213 225);</td><td style="color: rgb(203 213 225);">Lorem</td></tr>
    <tr><td>text-slate-400</td><td>color: rgb(148 163 184);</td><td style="color: rgb(148 163 184);">Lorem</td></tr>
    <tr><td>text-slate-500</td><td>color: rgb(100 116 139);</td><td style="color: rgb(100 116 139);">Lorem</td></tr>
    <tr><td>text-slate-600</td><td>color: rgb(71 85 105);</td><td style="color: rgb(71 85 105);">Lorem</td></tr>
    <tr><td>text-slate-700</td><td>color: rgb(51 65 85);</td><td style="color: rgb(51 65 85);">Lorem</td></tr>
    <tr><td>text-slate-800</td><td>color: rgb(30 41 59);</td><td style="color: rgb(30 41 59);">Lorem</td></tr>
    <tr><td>text-slate-900</td><td>color: rgb(15 23 42);</td><td style="color: rgb(15 23 42);">Lorem</td></tr>
    <tr><td>text-gray-50</td><td>color: rgb(249 250 251);</td><td style="color: rgb(249 250 251);">Lorem</td></tr>
    <tr><td>text-gray-100</td><td>color: rgb(243 244 246);</td><td style="color: rgb(243 244 246);">Lorem</td></tr>
    <tr><td>text-gray-200</td><td>color: rgb(229 231 235);</td><td style="color: rgb(229 231 235);">Lorem</td></tr>
    <tr><td>text-gray-300</td><td>color: rgb(209 213 219);</td><td style="color: rgb(209 213 219);">Lorem</td></tr>
    <tr><td>text-gray-400</td><td>color: rgb(156 163 175);</td><td style="color: rgb(156 163 175);">Lorem</td></tr>
    <tr><td>text-gray-500</td><td>color: rgb(107 114 128);</td><td style="color: rgb(107 114 128);">Lorem</td></tr>
    <tr><td>text-gray-600</td><td>color: rgb(75 85 99);</td><td style="color: rgb(75 85 99);">Lorem</td></tr>
    <tr><td>text-gray-700</td><td>color: rgb(55 65 81);</td><td style="color: rgb(55 65 81);">Lorem</td></tr>
    <tr><td>text-gray-800</td><td>color: rgb(31 41 55);</td><td style="color: rgb(31 41 55);">Lorem</td></tr>
    <tr><td>text-gray-900</td><td>color: rgb(17 24 39);</td><td style="color: rgb(17 24 39);">Lorem</td></tr>
    <tr><td>text-zinc-50</td><td>color: rgb(250 250 250);</td><td style="color: rgb(250 250 250);">Lorem</td></tr>
    <tr><td>text-zinc-100</td><td>color: rgb(244 244 245);</td><td style="color: rgb(244 244 245);">Lorem</td></tr>
    <tr><td>text-zinc-200</td><td>color: rgb(228 228 231);</td><td style="color: rgb(228 228 231);">Lorem</td></tr>
    <tr><td>text-zinc-300</td><td>color: rgb(212 212 216);</td><td style="color: rgb(212 212 216);">Lorem</td></tr>
    <tr><td>text-zinc-400</td><td>color: rgb(161 161 170);</td><td style="color: rgb(161 161 170);">Lorem</td></tr>
    <tr><td>text-zinc-500</td><td>color: rgb(113 113 122);</td><td style="color: rgb(113 113 122);">Lorem</td></tr>
    <tr><td>text-zinc-600</td><td>color: rgb(82 82 91);</td><td style="color: rgb(82 82 91);">Lorem</td></tr>
    <tr><td>text-zinc-700</td><td>color: rgb(63 63 70);</td><td style="color: rgb(63 63 70);">Lorem</td></tr>
    <tr><td>text-zinc-800</td><td>color: rgb(39 39 42);</td><td style="color: rgb(39 39 42);">Lorem</td></tr>
    <tr><td>text-zinc-900</td><td>color: rgb(24 24 27);</td><td style="color: rgb(24 24 27);">Lorem</td></tr>
    <tr><td>text-neutral-50</td><td>color: rgb(250 250 250);</td><td style="color: rgb(250 250 250);">Lorem</td></tr>
    <tr><td>text-neutral-100</td><td>color: rgb(245 245 245);</td><td style="color: rgb(245 245 245);">Lorem</td></tr>
    <tr><td>text-neutral-200</td><td>color: rgb(229 229 229);</td><td style="color: rgb(229 229 229);">Lorem</td></tr>
    <tr><td>text-neutral-300</td><td>color: rgb(212 212 212);</td><td style="color: rgb(212 212 212);">Lorem</td></tr>
    <tr><td>text-neutral-400</td><td>color: rgb(163 163 163);</td><td style="color: rgb(163 163 163);">Lorem</td></tr>
    <tr><td>text-neutral-500</td><td>color: rgb(115 115 115);</td><td style="color: rgb(115 115 115);">Lorem</td></tr>
    <tr><td>text-neutral-600</td><td>color: rgb(82 82 82);</td><td style="color: rgb(82 82 82);">Lorem</td></tr>
    <tr><td>text-neutral-700</td><td>color: rgb(64 64 64);</td><td style="color: rgb(64 64 64);">Lorem</td></tr>
    <tr><td>text-neutral-800</td><td>color: rgb(38 38 38);</td><td style="color: rgb(38 38 38);">Lorem</td></tr>
    <tr><td>text-neutral-900</td><td>color: rgb(23 23 23);</td><td style="color: rgb(23 23 23);">Lorem</td></tr>
    <tr><td>text-stone-50</td><td>color: rgb(250 250 249);</td><td style="color: rgb(250 250 249);">Lorem</td></tr>
    <tr><td>text-stone-100</td><td>color: rgb(245 245 244);</td><td style="color: rgb(245 245 244);">Lorem</td></tr>
    <tr><td>text-stone-200</td><td>color: rgb(231 229 228);</td><td style="color: rgb(231 229 228);">Lorem</td></tr>
    <tr><td>text-stone-300</td><td>color: rgb(214 211 209);</td><td style="color: rgb(214 211 209);">Lorem</td></tr>
    <tr><td>text-stone-400</td><td>color: rgb(168 162 158);</td><td style="color: rgb(168 162 158);">Lorem</td></tr>
    <tr><td>text-stone-500</td><td>color: rgb(120 113 108);</td><td style="color: rgb(120 113 108);">Lorem</td></tr>
    <tr><td>text-stone-600</td><td>color: rgb(87 83 78);</td><td style="color: rgb(87 83 78);">Lorem</td></tr>
    <tr><td>text-stone-700</td><td>color: rgb(68 64 60);</td><td style="color: rgb(68 64 60);">Lorem</td></tr>
    <tr><td>text-stone-800</td><td>color: rgb(41 37 36);</td><td style="color: rgb(41 37 36);">Lorem</td></tr>
    <tr><td>text-stone-900</td><td>color: rgb(28 25 23);</td><td style="color: rgb(28 25 23);">Lorem</td></tr>
    <tr><td>text-red-50</td><td>color: rgb(254 242 242);</td><td style="color: rgb(254 242 242);">Lorem</td></tr>
    <tr><td>text-red-100</td><td>color: rgb(254 226 226);</td><td style="color: rgb(254 226 226);">Lorem</td></tr>
    <tr><td>text-red-200</td><td>color: rgb(254 202 202);</td><td style="color: rgb(254 202 202);">Lorem</td></tr>
    <tr><td>text-red-300</td><td>color: rgb(252 165 165);</td><td style="color: rgb(252 165 165);">Lorem</td></tr>
    <tr><td>text-red-400</td><td>color: rgb(248 113 113);</td><td style="color: rgb(248 113 113);">Lorem</td></tr>
    <tr><td>text-red-500</td><td>color: rgb(239 68 68);</td><td style="color: rgb(239 68 68);">Lorem</td></tr>
    <tr><td>text-red-600</td><td>color: rgb(220 38 38);</td><td style="color: rgb(220 38 38);">Lorem</td></tr>
    <tr><td>text-red-700</td><td>color: rgb(185 28 28);</td><td style="color: rgb(185 28 28);">Lorem</td></tr>
    <tr><td>text-red-800</td><td>color: rgb(153 27 27);</td><td style="color: rgb(153 27 27);">Lorem</td></tr>
    <tr><td>text-red-900</td><td>color: rgb(127 29 29);</td><td style="color: rgb(127 29 29);">Lorem</td></tr>
    <tr><td>text-orange-50</td><td>color: rgb(255 247 237);</td><td style="color: rgb(255 247 237);">Lorem</td></tr>
    <tr><td>text-orange-100</td><td>color: rgb(255 237 213);</td><td style="color: rgb(255 237 213);">Lorem</td></tr>
    <tr><td>text-orange-200</td><td>color: rgb(254 215 170);</td><td style="color: rgb(254 215 170);">Lorem</td></tr>
    <tr><td>text-orange-300</td><td>color: rgb(253 186 116);</td><td style="color: rgb(253 186 116);">Lorem</td></tr>
    <tr><td>text-orange-400</td><td>color: rgb(251 146 60);</td><td style="color: rgb(251 146 60);">Lorem</td></tr>
    <tr><td>text-orange-500</td><td>color: rgb(249 115 22);</td><td style="color: rgb(249 115 22);">Lorem</td></tr>
    <tr><td>text-orange-600</td><td>color: rgb(234 88 12);</td><td style="color: rgb(234 88 12);">Lorem</td></tr>
    <tr><td>text-orange-700</td><td>color: rgb(194 65 12);</td><td style="color: rgb(194 65 12);">Lorem</td></tr>
    <tr><td>text-orange-800</td><td>color: rgb(154 52 18);</td><td style="color: rgb(154 52 18);">Lorem</td></tr>
    <tr><td>text-orange-900</td><td>color: rgb(124 45 18);</td><td style="color: rgb(124 45 18);">Lorem</td></tr>
    <tr><td>text-amber-50</td><td>color: rgb(255 251 235);</td><td style="color: rgb(255 251 235);">Lorem</td></tr>
    <tr><td>text-amber-100</td><td>color: rgb(254 243 199);</td><td style="color: rgb(254 243 199);">Lorem</td></tr>
    <tr><td>text-amber-200</td><td>color: rgb(253 230 138);</td><td style="color: rgb(253 230 138);">Lorem</td></tr>
    <tr><td>text-amber-300</td><td>color: rgb(252 211 77);</td><td style="color: rgb(252 211 77);">Lorem</td></tr>
    <tr><td>text-amber-400</td><td>color: rgb(251 191 36);</td><td style="color: rgb(251 191 36);">Lorem</td></tr>
    <tr><td>text-amber-500</td><td>color: rgb(245 158 11);</td><td style="color: rgb(245 158 11);">Lorem</td></tr>
    <tr><td>text-amber-600</td><td>color: rgb(217 119 6);</td><td style="color: rgb(217 119 6);">Lorem</td></tr>
    <tr><td>text-amber-700</td><td>color: rgb(180 83 9);</td><td style="color: rgb(180 83 9);">Lorem</td></tr>
    <tr><td>text-amber-800</td><td>color: rgb(146 64 14);</td><td style="color: rgb(146 64 14);">Lorem</td></tr>
    <tr><td>text-amber-900</td><td>color: rgb(120 53 15);</td><td style="color: rgb(120 53 15);">Lorem</td></tr>
    <tr><td>text-yellow-50</td><td>color: rgb(254 252 232);</td><td style="color: rgb(254 252 232);">Lorem</td></tr>
    <tr><td>text-yellow-100</td><td>color: rgb(254 249 195);</td><td style="color: rgb(254 249 195);">Lorem</td></tr>
    <tr><td>text-yellow-200</td><td>color: rgb(254 240 138);</td><td style="color: rgb(254 240 138);">Lorem</td></tr>
    <tr><td>text-yellow-300</td><td>color: rgb(253 224 71);</td><td style="color: rgb(253 224 71);">Lorem</td></tr>
    <tr><td>text-yellow-400</td><td>color: rgb(250 204 21);</td><td style="color: rgb(250 204 21);">Lorem</td></tr>
    <tr><td>text-yellow-500</td><td>color: rgb(234 179 8);</td><td style="color: rgb(234 179 8);">Lorem</td></tr>
    <tr><td>text-yellow-600</td><td>color: rgb(202 138 4);</td><td style="color: rgb(202 138 4);">Lorem</td></tr>
    <tr><td>text-yellow-700</td><td>color: rgb(161 98 7);</td><td style="color: rgb(161 98 7);">Lorem</td></tr>
    <tr><td>text-yellow-800</td><td>color: rgb(133 77 14);</td><td style="color: rgb(133 77 14);">Lorem</td></tr>
    <tr><td>text-yellow-900</td><td>color: rgb(113 63 18);</td><td style="color: rgb(113 63 18);">Lorem</td></tr>
    <tr><td>text-lime-50</td><td>color: rgb(247 254 231);</td><td style="color: rgb(247 254 231);">Lorem</td></tr>
    <tr><td>text-lime-100</td><td>color: rgb(236 252 203);</td><td style="color: rgb(236 252 203);">Lorem</td></tr>
    <tr><td>text-lime-200</td><td>color: rgb(217 249 157);</td><td style="color: rgb(217 249 157);">Lorem</td></tr>
    <tr><td>text-lime-300</td><td>color: rgb(190 242 100);</td><td style="color: rgb(190 242 100);">Lorem</td></tr>
    <tr><td>text-lime-400</td><td>color: rgb(163 230 53);</td><td style="color: rgb(163 230 53);">Lorem</td></tr>
    <tr><td>text-lime-500</td><td>color: rgb(132 204 22);</td><td style="color: rgb(132 204 22);">Lorem</td></tr>
    <tr><td>text-lime-600</td><td>color: rgb(101 163 13);</td><td style="color: rgb(101 163 13);">Lorem</td></tr>
    <tr><td>text-lime-700</td><td>color: rgb(77 124 15);</td><td style="color: rgb(77 124 15);">Lorem</td></tr>
    <tr><td>text-lime-800</td><td>color: rgb(63 98 18);</td><td style="color: rgb(63 98 18);">Lorem</td></tr>
    <tr><td>text-lime-900</td><td>color: rgb(54 83 20);</td><td style="color: rgb(54 83 20);">Lorem</td></tr>
    <tr><td>text-green-50</td><td>color: rgb(240 253 244);</td><td style="color: rgb(240 253 244);">Lorem</td></tr>
    <tr><td>text-green-100</td><td>color: rgb(220 252 231);</td><td style="color: rgb(220 252 231);">Lorem</td></tr>
    <tr><td>text-green-200</td><td>color: rgb(187 247 208);</td><td style="color: rgb(187 247 208);">Lorem</td></tr>
    <tr><td>text-green-300</td><td>color: rgb(134 239 172);</td><td style="color: rgb(134 239 172);">Lorem</td></tr>
    <tr><td>text-green-400</td><td>color: rgb(74 222 128);</td><td style="color: rgb(74 222 128);">Lorem</td></tr>
    <tr><td>text-green-500</td><td>color: rgb(34 197 94);</td><td style="color: rgb(34 197 94);">Lorem</td></tr>
    <tr><td>text-green-600</td><td>color: rgb(22 163 74);</td><td style="color: rgb(22 163 74);">Lorem</td></tr>
    <tr><td>text-green-700</td><td>color: rgb(21 128 61);</td><td style="color: rgb(21 128 61);">Lorem</td></tr>
    <tr><td>text-green-800</td><td>color: rgb(22 101 52);</td><td style="color: rgb(22 101 52);">Lorem</td></tr>
    <tr><td>text-green-900</td><td>color: rgb(20 83 45);</td><td style="color: rgb(20 83 45);">Lorem</td></tr>
    <tr><td>text-emerald-50</td><td>color: rgb(236 253 245);</td><td style="color: rgb(236 253 245);">Lorem</td></tr>
    <tr><td>text-emerald-100</td><td>color: rgb(209 250 229);</td><td style="color: rgb(209 250 229);">Lorem</td></tr>
    <tr><td>text-emerald-200</td><td>color: rgb(167 243 208);</td><td style="color: rgb(167 243 208);">Lorem</td></tr>
    <tr><td>text-emerald-300</td><td>color: rgb(110 231 183);</td><td style="color: rgb(110 231 183);">Lorem</td></tr>
    <tr><td>text-emerald-400</td><td>color: rgb(52 211 153);</td><td style="color: rgb(52 211 153);">Lorem</td></tr>
    <tr><td>text-emerald-500</td><td>color: rgb(16 185 129);</td><td style="color: rgb(16 185 129);">Lorem</td></tr>
    <tr><td>text-emerald-600</td><td>color: rgb(5 150 105);</td><td style="color: rgb(5 150 105);">Lorem</td></tr>
    <tr><td>text-emerald-700</td><td>color: rgb(4 120 87);</td><td style="color: rgb(4 120 87);">Lorem</td></tr>
    <tr><td>text-emerald-800</td><td>color: rgb(6 95 70);</td><td style="color: rgb(6 95 70);">Lorem</td></tr>
    <tr><td>text-emerald-900</td><td>color: rgb(6 78 59);</td><td style="color: rgb(6 78 59);">Lorem</td></tr>
    <tr><td>text-teal-50</td><td>color: rgb(240 253 250);</td><td style="color: rgb(240 253 250);">Lorem</td></tr>
    <tr><td>text-teal-100</td><td>color: rgb(204 251 241);</td><td style="color: rgb(204 251 241);">Lorem</td></tr>
    <tr><td>text-teal-200</td><td>color: rgb(153 246 228);</td><td style="color: rgb(153 246 228);">Lorem</td></tr>
    <tr><td>text-teal-300</td><td>color: rgb(94 234 212);</td><td style="color: rgb(94 234 212);">Lorem</td></tr>
    <tr><td>text-teal-400</td><td>color: rgb(45 212 191);</td><td style="color: rgb(45 212 191);">Lorem</td></tr>
    <tr><td>text-teal-500</td><td>color: rgb(20 184 166);</td><td style="color: rgb(20 184 166);">Lorem</td></tr>
    <tr><td>text-teal-600</td><td>color: rgb(13 148 136);</td><td style="color: rgb(13 148 136);">Lorem</td></tr>
    <tr><td>text-teal-700</td><td>color: rgb(15 118 110);</td><td style="color: rgb(15 118 110);">Lorem</td></tr>
    <tr><td>text-teal-800</td><td>color: rgb(17 94 89);</td><td style="color: rgb(17 94 89);">Lorem</td></tr>
    <tr><td>text-teal-900</td><td>color: rgb(19 78 74);</td><td style="color: rgb(19 78 74);">Lorem</td></tr>
    <tr><td>text-cyan-50</td><td>color: rgb(236 254 255);</td><td style="color: rgb(236 254 255);">Lorem</td></tr>
    <tr><td>text-cyan-100</td><td>color: rgb(207 250 254);</td><td style="color: rgb(207 250 254);">Lorem</td></tr>
    <tr><td>text-cyan-200</td><td>color: rgb(165 243 252);</td><td style="color: rgb(165 243 252);">Lorem</td></tr>
    <tr><td>text-cyan-300</td><td>color: rgb(103 232 249);</td><td style="color: rgb(103 232 249);">Lorem</td></tr>
    <tr><td>text-cyan-400</td><td>color: rgb(34 211 238);</td><td style="color: rgb(34 211 238);">Lorem</td></tr>
    <tr><td>text-cyan-500</td><td>color: rgb(6 182 212);</td><td style="color: rgb(6 182 212);">Lorem</td></tr>
    <tr><td>text-cyan-600</td><td>color: rgb(8 145 178);</td><td style="color: rgb(8 145 178);">Lorem</td></tr>
    <tr><td>text-cyan-700</td><td>color: rgb(14 116 144);</td><td style="color: rgb(14 116 144);">Lorem</td></tr>
    <tr><td>text-cyan-800</td><td>color: rgb(21 94 117);</td><td style="color: rgb(21 94 117);">Lorem</td></tr>
    <tr><td>text-cyan-900</td><td>color: rgb(22 78 99);</td><td style="color: rgb(22 78 99);">Lorem</td></tr>
    <tr><td>text-sky-50</td><td>color: rgb(240 249 255);</td><td style="color: rgb(240 249 255);">Lorem</td></tr>
    <tr><td>text-sky-100</td><td>color: rgb(224 242 254);</td><td style="color: rgb(224 242 254);">Lorem</td></tr>
    <tr><td>text-sky-200</td><td>color: rgb(186 230 253);</td><td style="color: rgb(186 230 253);">Lorem</td></tr>
    <tr><td>text-sky-300</td><td>color: rgb(125 211 252);</td><td style="color: rgb(125 211 252);">Lorem</td></tr>
    <tr><td>text-sky-400</td><td>color: rgb(56 189 248);</td><td style="color: rgb(56 189 248);">Lorem</td></tr>
    <tr><td>text-sky-500</td><td>color: rgb(14 165 233);</td><td style="color: rgb(14 165 233);">Lorem</td></tr>
    <tr><td>text-sky-600</td><td>color: rgb(2 132 199);</td><td style="color: rgb(2 132 199);">Lorem</td></tr>
    <tr><td>text-sky-700</td><td>color: rgb(3 105 161);</td><td style="color: rgb(3 105 161);">Lorem</td></tr>
    <tr><td>text-sky-800</td><td>color: rgb(7 89 133);</td><td style="color: rgb(7 89 133);">Lorem</td></tr>
    <tr><td>text-sky-900</td><td>color: rgb(12 74 110);</td><td style="color: rgb(12 74 110);">Lorem</td></tr>
    <tr><td>text-blue-50</td><td>color: rgb(239 246 255);</td><td style="color: rgb(239 246 255);">Lorem</td></tr>
    <tr><td>text-blue-100</td><td>color: rgb(219 234 254);</td><td style="color: rgb(219 234 254);">Lorem</td></tr>
    <tr><td>text-blue-200</td><td>color: rgb(191 219 254);</td><td style="color: rgb(191 219 254);">Lorem</td></tr>
    <tr><td>text-blue-300</td><td>color: rgb(147 197 253);</td><td style="color: rgb(147 197 253);">Lorem</td></tr>
    <tr><td>text-blue-400</td><td>color: rgb(96 165 250);</td><td style="color: rgb(96 165 250);">Lorem</td></tr>
    <tr><td>text-blue-500</td><td>color: rgb(59 130 246);</td><td style="color: rgb(59 130 246);">Lorem</td></tr>
    <tr><td>text-blue-600</td><td>color: rgb(37 99 235);</td><td style="color: rgb(37 99 235);">Lorem</td></tr>
    <tr><td>text-blue-700</td><td>color: rgb(29 78 216);</td><td style="color: rgb(29 78 216);">Lorem</td></tr>
    <tr><td>text-blue-800</td><td>color: rgb(30 64 175);</td><td style="color: rgb(30 64 175);">Lorem</td></tr>
    <tr><td>text-blue-900</td><td>color: rgb(30 58 138);</td><td style="color: rgb(30 58 138);">Lorem</td></tr>
    <tr><td>text-indigo-50</td><td>color: rgb(238 242 255);</td><td style="color: rgb(238 242 255);">Lorem</td></tr>
    <tr><td>text-indigo-100</td><td>color: rgb(224 231 255);</td><td style="color: rgb(224 231 255);">Lorem</td></tr>
    <tr><td>text-indigo-200</td><td>color: rgb(199 210 254);</td><td style="color: rgb(199 210 254);">Lorem</td></tr>
    <tr><td>text-indigo-300</td><td>color: rgb(165 180 252);</td><td style="color: rgb(165 180 252);">Lorem</td></tr>
    <tr><td>text-indigo-400</td><td>color: rgb(129 140 248);</td><td style="color: rgb(129 140 248);">Lorem</td></tr>
    <tr><td>text-indigo-500</td><td>color: rgb(99 102 241);</td><td style="color: rgb(99 102 241);">Lorem</td></tr>
    <tr><td>text-indigo-600</td><td>color: rgb(79 70 229);</td><td style="color: rgb(79 70 229);">Lorem</td></tr>
    <tr><td>text-indigo-700</td><td>color: rgb(67 56 202);</td><td style="color: rgb(67 56 202);">Lorem</td></tr>
    <tr><td>text-indigo-800</td><td>color: rgb(55 48 163);</td><td style="color: rgb(55 48 163);">Lorem</td></tr>
    <tr><td>text-indigo-900</td><td>color: rgb(49 46 129);</td><td style="color: rgb(49 46 129);">Lorem</td></tr>
    <tr><td>text-violet-50</td><td>color: rgb(245 243 255);</td><td style="color: rgb(245 243 255);">Lorem</td></tr>
    <tr><td>text-violet-100</td><td>color: rgb(237 233 254);</td><td style="color: rgb(237 233 254);">Lorem</td></tr>
    <tr><td>text-violet-200</td><td>color: rgb(221 214 254);</td><td style="color: rgb(221 214 254);">Lorem</td></tr>
    <tr><td>text-violet-300</td><td>color: rgb(196 181 253);</td><td style="color: rgb(196 181 253);">Lorem</td></tr>
    <tr><td>text-violet-400</td><td>color: rgb(167 139 250);</td><td style="color: rgb(167 139 250);">Lorem</td></tr>
    <tr><td>text-violet-500</td><td>color: rgb(139 92 246);</td><td style="color: rgb(139 92 246);">Lorem</td></tr>
    <tr><td>text-violet-600</td><td>color: rgb(124 58 237);</td><td style="color: rgb(124 58 237);">Lorem</td></tr>
    <tr><td>text-violet-700</td><td>color: rgb(109 40 217);</td><td style="color: rgb(109 40 217);">Lorem</td></tr>
    <tr><td>text-violet-800</td><td>color: rgb(91 33 182);</td><td style="color: rgb(91 33 182);">Lorem</td></tr>
    <tr><td>text-violet-900</td><td>color: rgb(76 29 149);</td><td style="color: rgb(76 29 149);">Lorem</td></tr>
    <tr><td>text-purple-50</td><td>color: rgb(250 245 255);</td><td style="color: rgb(250 245 255);">Lorem</td></tr>
    <tr><td>text-purple-100</td><td>color: rgb(243 232 255);</td><td style="color: rgb(243 232 255);">Lorem</td></tr>
    <tr><td>text-purple-200</td><td>color: rgb(233 213 255);</td><td style="color: rgb(233 213 255);">Lorem</td></tr>
    <tr><td>text-purple-300</td><td>color: rgb(216 180 254);</td><td style="color: rgb(216 180 254);">Lorem</td></tr>
    <tr><td>text-purple-400</td><td>color: rgb(192 132 252);</td><td style="color: rgb(192 132 252);">Lorem</td></tr>
    <tr><td>text-purple-500</td><td>color: rgb(168 85 247);</td><td style="color: rgb(168 85 247);">Lorem</td></tr>
    <tr><td>text-purple-600</td><td>color: rgb(147 51 234);</td><td style="color: rgb(147 51 234);">Lorem</td></tr>
    <tr><td>text-purple-700</td><td>color: rgb(126 34 206);</td><td style="color: rgb(126 34 206);">Lorem</td></tr>
    <tr><td>text-purple-800</td><td>color: rgb(107 33 168);</td><td style="color: rgb(107 33 168);">Lorem</td></tr>
    <tr><td>text-purple-900</td><td>color: rgb(88 28 135);</td><td style="color: rgb(88 28 135);">Lorem</td></tr>
    <tr><td>text-fuchsia-50</td><td>color: rgb(253 244 255);</td><td style="color: rgb(253 244 255);">Lorem</td></tr>
    <tr><td>text-fuchsia-100</td><td>color: rgb(250 232 255);</td><td style="color: rgb(250 232 255);">Lorem</td></tr>
    <tr><td>text-fuchsia-200</td><td>color: rgb(245 208 254);</td><td style="color: rgb(245 208 254);">Lorem</td></tr>
    <tr><td>text-fuchsia-300</td><td>color: rgb(240 171 252);</td><td style="color: rgb(240 171 252);">Lorem</td></tr>
    <tr><td>text-fuchsia-400</td><td>color: rgb(232 121 249);</td><td style="color: rgb(232 121 249);">Lorem</td></tr>
    <tr><td>text-fuchsia-500</td><td>color: rgb(217 70 239);</td><td style="color: rgb(217 70 239);">Lorem</td></tr>
    <tr><td>text-fuchsia-600</td><td>color: rgb(192 38 211);</td><td style="color: rgb(192 38 211);">Lorem</td></tr>
    <tr><td>text-fuchsia-700</td><td>color: rgb(162 28 175);</td><td style="color: rgb(162 28 175);">Lorem</td></tr>
    <tr><td>text-fuchsia-800</td><td>color: rgb(134 25 143);</td><td style="color: rgb(134 25 143);">Lorem</td></tr>
    <tr><td>text-fuchsia-900</td><td>color: rgb(112 26 117);</td><td style="color: rgb(112 26 117);">Lorem</td></tr>
    <tr><td>text-pink-50</td><td>color: rgb(253 242 248);</td><td style="color: rgb(253 242 248);">Lorem</td></tr>
    <tr><td>text-pink-100</td><td>color: rgb(252 231 243);</td><td style="color: rgb(252 231 243);">Lorem</td></tr>
    <tr><td>text-pink-200</td><td>color: rgb(251 207 232);</td><td style="color: rgb(251 207 232);">Lorem</td></tr>
    <tr><td>text-pink-300</td><td>color: rgb(249 168 212);</td><td style="color: rgb(249 168 212);">Lorem</td></tr>
    <tr><td>text-pink-400</td><td>color: rgb(244 114 182);</td><td style="color: rgb(244 114 182);">Lorem</td></tr>
    <tr><td>text-pink-500</td><td>color: rgb(236 72 153);</td><td style="color: rgb(236 72 153);">Lorem</td></tr>
    <tr><td>text-pink-600</td><td>color: rgb(219 39 119);</td><td style="color: rgb(219 39 119);">Lorem</td></tr>
    <tr><td>text-pink-700</td><td>color: rgb(190 24 93);</td><td style="color: rgb(190 24 93);">Lorem</td></tr>
    <tr><td>text-pink-800</td><td>color: rgb(157 23 77);</td><td style="color: rgb(157 23 77);">Lorem</td></tr>
    <tr><td>text-pink-900</td><td>color: rgb(131 24 67);</td><td style="color: rgb(131 24 67);">Lorem</td></tr>
    <tr><td>text-rose-50</td><td>color: rgb(255 241 242);</td><td style="color: rgb(255 241 242);">Lorem</td></tr>
    <tr><td>text-rose-100</td><td>color: rgb(255 228 230);</td><td style="color: rgb(255 228 230);">Lorem</td></tr>
    <tr><td>text-rose-200</td><td>color: rgb(254 205 211);</td><td style="color: rgb(254 205 211);">Lorem</td></tr>
    <tr><td>text-rose-300</td><td>color: rgb(253 164 175);</td><td style="color: rgb(253 164 175);">Lorem</td></tr>
    <tr><td>text-rose-400</td><td>color: rgb(251 113 133);</td><td style="color: rgb(251 113 133);">Lorem</td></tr>
    <tr><td>text-rose-500</td><td>color: rgb(244 63 94);</td><td style="color: rgb(244 63 94);">Lorem</td></tr>
    <tr><td>text-rose-600</td><td>color: rgb(225 29 72);</td><td style="color: rgb(225 29 72);">Lorem</td></tr>
    <tr><td>text-rose-700</td><td>color: rgb(190 18 60);</td><td style="color: rgb(190 18 60);">Lorem</td></tr>
    <tr><td>text-rose-800</td><td>color: rgb(159 18 57);</td><td style="color: rgb(159 18 57);">Lorem</td></tr>
    <tr><td>text-rose-900</td><td>color: rgb(136 19 55);</td><td style="color: rgb(136 19 55);">Lorem</td></tr>
  </tbody>
</table>

### Note about the opacity syntax

The color opacity can be changed using the new syntax: e.g. `text-red-500/25` will generate
`color: rgb(239 68 68 / 0.25);`.

### Arbitrary values

Any [`<color>`](crate::utils::value_matchers::is_matching_color) property is allowed as arbitrary value.
For example, `text-[red]`.

[Tailwind reference](https://tailwindcss.com/docs/text-color)
