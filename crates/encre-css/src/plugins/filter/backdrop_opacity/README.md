Utilities for controlling the opacity of an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>backdrop-opacity-<i>&lt;integer&gt;</i></td><td>backdrop-filter: opacity(<i>&lt;integer / 100&gt;</i>);</td></tr>
  </tbody>
</table>

### Tailwind compatibility

Opacity values don't follow Tailwind's philosophy of limiting possible values and all values
from 0 to 100 are supported. They are however perfectly compatible with Tailwind's values.

[Tailwind reference](https://tailwindcss.com/docs/opacity)
