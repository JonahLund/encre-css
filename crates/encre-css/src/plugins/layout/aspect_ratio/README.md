Utilities for controlling the aspect ratio of an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>aspect-auto</td><td>aspect-ratio: auto;</td></tr>
    <tr><td>aspect-square</td><td>aspect-ratio: 1 / 1;</td></tr>
    <tr><td>aspect-video</td><td>aspect-ratio: 16 / 9;</td></tr>
  </tbody>
</table>

### Arbitrary values

Any property is allowed as arbitrary value.
For example, `aspect-[0.5]`.

[Tailwind reference](https://tailwindcss.com/docs/aspect-ratio)
