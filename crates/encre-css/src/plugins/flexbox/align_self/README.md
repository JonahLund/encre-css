Utilities for controlling how an individual flex or grid item is positioned along its container's cross axis.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>self-auto</td>
      <td>align-self: auto;</td>
    </tr>
    <tr>
      <td>self-start</td>
      <td>align-self: flex-start;</td>
    </tr>
    <tr>
      <td>self-end</td>
      <td>align-self: flex-end;</td>
    </tr>
    <tr>
      <td>self-center</td>
      <td>align-self: center;</td>
    </tr>
    <tr>
      <td>self-stretch</td>
      <td>align-self: stretch;</td>
    </tr>
    <tr>
      <td>self-baseline</td>
      <td>align-self: baseline;</td>
    </tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/align-self)
