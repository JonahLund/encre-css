Utilities for controlling the display box type of an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>block</td><td>display: block;</td></tr>
    <tr><td>inline-block</td><td>display: inline-block;</td></tr>
    <tr><td>inline</td><td>display: inline;</td></tr>
    <tr><td>flex</td><td>display: flex;</td></tr>
    <tr><td>inline-flex</td><td>display: inline-flex;</td></tr>
    <tr><td>table</td><td>display: table;</td></tr>
    <tr><td>inline-table</td><td>display: inline-table;</td></tr>
    <tr><td>table-caption</td><td>display: table-caption;</td></tr>
    <tr><td>table-cell</td><td>display: table-cell;</td></tr>
    <tr><td>table-column</td><td>display: table-column;</td></tr>
    <tr><td>table-column-group</td><td>display: table-column-group;</td></tr>
    <tr><td>table-footer-group</td><td>display: table-footer-group;</td></tr>
    <tr><td>table-header-group</td><td>display: table-header-group;</td></tr>
    <tr><td>table-row-group</td><td>display: table-row-group;</td></tr>
    <tr><td>table-row</td><td>display: table-row;</td></tr>
    <tr><td>flow-root</td><td>display: flow-root;</td></tr>
    <tr><td>grid</td><td>display: grid;</td></tr>
    <tr><td>inline-grid</td><td>display: inline-grid;</td></tr>
    <tr><td>contents</td><td>display: contents;</td></tr>
    <tr><td>list-item</td><td>display: list-item;</td></tr>
    <tr><td>hidden</td><td>display: none;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/display)
