Utilities for controlling the bounding box of an element's background.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>bg-clip-border</td><td>background-clip: border-box;</td></tr>
    <tr><td>bg-clip-padding</td><td>background-clip: padding-box;</td></tr>
    <tr><td>bg-clip-content</td><td>background-clip: content-box;</td></tr>
    <tr><td>bg-clip-text</td><td>background-clip: text;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/background-clip)
