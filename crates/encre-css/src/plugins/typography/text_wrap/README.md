Utilities for controlling how text wraps within an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>text-wrap</td><td>text-wrap: wrap;</td></tr>
    <tr><td>text-nowrap</td><td>text-wrap: nowrap;</td></tr>
    <tr><td>text-balance</td><td>text-wrap: balance</td></tr>
    <tr><td>text-pretty</td><td>text-wrap: pretty;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/text-wrap)
