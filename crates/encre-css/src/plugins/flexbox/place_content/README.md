Utilities for controlling how content is justified and aligned at the same time.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>place-content-center</td>
      <td>place-content: center;</td>
    </tr>
    <tr>
      <td>place-content-start</td>
      <td>place-content: start;</td>
    </tr>
    <tr>
      <td>place-content-end</td>
      <td>place-content: end;</td>
    </tr>
    <tr>
      <td>place-content-between</td>
      <td>place-content: space-between;</td>
    </tr>
    <tr>
      <td>place-content-around</td>
      <td>place-content: space-around;</td>
    </tr>
    <tr>
      <td>place-content-evenly</td>
      <td>place-content: space-evenly;</td>
    </tr>
    <tr>
      <td>place-content-stretch</td>
      <td>place-content: space-stretch;</td>
    </tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/place-content)
