Utilities for controlling how strictly snap points are enforced in a snap container.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>snap-none</td><td>scroll-snap-type: none;</td></tr>
    <tr><td>snap-x</td><td>scroll-snap-type: x var(--en-scroll-snap-strictness);</td></tr>
    <tr><td>snap-y</td><td>scroll-snap-type: y var(--en-scroll-snap-strictness);</td></tr>
    <tr><td>snap-both</td><td>scroll-snap-type: both var(--en-scroll-snap-strictness);</td></tr>
    <tr><td>snap-mandatory</td><td>--en-scroll-snap-strictness: mandatory;</td></tr>
    <tr><td>snap-proximity</td><td>--en-scroll-snap-strictness: proximity;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/scroll-snap-type)
