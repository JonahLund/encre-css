Utilities for controlling the transformation of text.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>uppercase</td><td>text-transform: uppercase;</td></tr>
    <tr><td>lowercase</td><td>text-transform: lowercase;</td></tr>
    <tr><td>capitalize</td><td>text-transform: capitalize;</td></tr>
    <tr><td>normal-case</td><td>text-transform: none;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/text-transform)
