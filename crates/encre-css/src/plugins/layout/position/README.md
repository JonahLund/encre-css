Utilities for controlling how an element is positioned in the DOM.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>static</td><td>position: static;</td></tr>
    <tr><td>fixed</td><td>position: fixed;</td></tr>
    <tr><td>absolute</td><td>position: absolute;</td></tr>
    <tr><td>relative</td><td>position: relative;</td></tr>
    <tr><td>sticky</td><td>position: sticky;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/position)
