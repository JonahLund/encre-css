Utilities for enabling and disabling backdrop filters on an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>backdrop-filter</td><td>filter: var(--en-backdrop-blur) var(--en-backdrop-brightness) var(--en-backdrop-contrast) var(--en-backdrop-grayscale) var(--en-backdrop-hue-rotate) var(--en-backdrop-invert) var(--en-backdrop-saturate) var(--en-backdrop-sepia) var(--en-backdrop-drop-shadow);</td></tr>
    <tr><td>backdrop-filter-none</td><td>filter: none;</td></tr>
  </tbody>
</table>

### Deprecation notice

It is not required to use the `backdrop-filter` class to use backdrop filters, this function
only mostly for compatibility reasons.
