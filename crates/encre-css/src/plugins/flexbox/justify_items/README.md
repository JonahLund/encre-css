Utilities for controlling how grid items are aligned along their inline axis.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>justify-items-start</td>
      <td>justify-items: start;</td>
    </tr>
    <tr>
      <td>justify-items-end</td>
      <td>justify-items: end;</td>
    </tr>
    <tr>
      <td>justify-items-center</td>
      <td>justify-items: center;</td>
    </tr>
    <tr>
      <td>justify-items-stretch</td>
      <td>justify-items: stretch;</td>
    </tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/justify-items)
