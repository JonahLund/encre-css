Utilities for applying grayscale filters to an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>grayscale-<i>&lt;integer&gt;</i></td><td>filter: grayscale(<i>&lt;integer / 100&gt;</i>);</td></tr>
    <tr><td>grayscale</td><td>filter: grayscale(100%);</td></tr>
  </tbody>
</table>

### Tailwind compatibility

Grayscale values don't follow Tailwind's philosophy of limiting possible values and all
numbers are supported. They are however perfectly compatible with Tailwind's values.

[Tailwind reference](https://tailwindcss.com/docs/grayscale)
