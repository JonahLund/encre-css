Utilities for controlling the visibility of an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>visible</td><td>visibility: visible;</td></tr>
    <tr><td>invisible</td><td>visibility: hidden;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/visibility)
