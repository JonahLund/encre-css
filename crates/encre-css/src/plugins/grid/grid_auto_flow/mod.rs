#![doc = include_str!("README.md")]
#![doc(alias = "grid")]
use crate::prelude::build_plugin::*;

#[derive(Debug)]
pub(crate) struct PluginDefinition;

impl Plugin for PluginDefinition {
    fn can_handle(&self, context: ContextCanHandle) -> bool {
        matches!(
            context.modifier,
            Modifier::Builtin {
                value: "row" | "col" | "dense" | "row-dense" | "col-dense",
                ..
            }
        )
    }

    fn handle(&self, context: &mut ContextHandle) {
        if let Modifier::Builtin { value, .. } = context.modifier {
            context.buffer.line(format_args!(
                "grid-auto-flow: {};",
                match *value {
                    "col" => "column",
                    "row-dense" => "row dense",
                    "col-dense" => "column dense",
                    _ => value,
                }
            ));
        }
    }
}
