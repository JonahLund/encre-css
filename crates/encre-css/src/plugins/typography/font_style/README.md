Utilities for controlling the style of text.

<table style="display: table;">
    <thead>
        <tr>
            <th style="text-align: center;">Class</th>
            <th style="text-align: center;">Properties</th>
        </tr>
    </thead>
    <tbody>
        <tr><td>italic</td><td>font-style: italic;</td></tr>
        <tr><td>not-italic</td><td>font-style: normal;</td></tr>
    </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/font-style)
