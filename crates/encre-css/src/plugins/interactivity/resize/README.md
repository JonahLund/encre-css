Utilities for controlling how an element can be resized.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>resize-none</td><td>resize: none;</td></tr>
    <tr><td>resize-y</td><td>resize: vertical;</td></tr>
    <tr><td>resize-x</td><td>resize: horizontal;</td></tr>
    <tr><td>resize</td><td>resize: both;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/resize)
