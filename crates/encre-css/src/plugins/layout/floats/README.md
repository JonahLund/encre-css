Utilities for controlling the wrapping of content around an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>float-right</td><td>float: right;</td></tr>
    <tr><td>float-left</td><td>float: left;</td></tr>
    <tr><td>float-none</td><td>float: none;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/float)
