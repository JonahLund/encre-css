Utilities for controlling the wrapping of content around an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>clear-left</td><td>clear: left;</td></tr>
    <tr><td>clear-right</td><td>clear: right;</td></tr>
    <tr><td>clear-both</td><td>clear: both;</td></tr>
    <tr><td>clear-none</td><td>clear: none;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/clear)
