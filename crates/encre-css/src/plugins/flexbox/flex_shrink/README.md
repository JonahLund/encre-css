Utilities for controlling how flex items shrink.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>shrink-<i>&lt;integer&gt;</i></td><td>flex-shrink: <i>&lt;integer&gt;</i>;</td></tr>
    <tr><td>shrink</td><td>flex-shrink: 1;</td></tr>
  </tbody>
</table>

### Tailwind compatibility

Flex shrink values don't follow Tailwind's philosophy of limiting possible values and all numbers
are supported. They are however perfectly compatible with Tailwind's values.

[Tailwind reference](https://tailwindcss.com/docs/flex-shrink)
