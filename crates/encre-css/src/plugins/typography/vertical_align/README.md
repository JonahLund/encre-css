Utilities for controlling the vertical alignment of an inline or table-cell box.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>align-baseline</td><td>vertical-align: baseline;</td></tr>
    <tr><td>align-top</td><td>vertical-align: top;</td></tr>
    <tr><td>align-middle</td><td>vertical-align: middle;</td></tr>
    <tr><td>align-bottom</td><td>vertical-align: bottom;</td></tr>
    <tr><td>align-text-top</td><td>vertical-align: text-top;</td></tr>
    <tr><td>align-text-bottom</td><td>vertical-align: text-bottom;</td></tr>
    <tr><td>align-sub</td><td>vertical-align: sub;</td></tr>
    <tr><td>align-super</td><td>vertical-align: super;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/vartical-align)
