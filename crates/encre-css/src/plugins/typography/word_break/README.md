Utilities for controlling word breaks in an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>break-normal</td><td>overflow-wrap: normal;<br>word-break: normal;</td></tr>
    <tr><td>break-words</td><td>overflow-wrap: break-word;</td></tr>
    <tr><td>break-all</td><td>word-break: break-all;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/word-break)
