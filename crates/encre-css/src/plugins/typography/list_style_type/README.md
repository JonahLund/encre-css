Utilities for controlling the bullet/number style of a list.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>list-none</td><td>list-style-type: none;</td></tr>
    <tr><td>list-disc</td><td>list-style-type: disc;</td></tr>
    <tr><td>list-decimal</td><td>list-style-type: decimal;</td></tr>
  </tbody>
</table>

### Arbitrary values

Any property is allowed as arbitrary value.
For example, `list-[upper-roman]`.

[Tailwind reference](https://tailwindcss.com/docs/list-style-type)
