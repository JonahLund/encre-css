Utilities for enabling and disabling filters on an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>filter</td><td>filter: var(--en-blur) var(--en-brightness) var(--en-contrast) var(--en-grayscale) var(--en-hue-rotate) var(--en-invert) var(--en-saturate) var(--en-sepia) var(--en-drop-shadow);</td></tr>
    <tr><td>filter-none</td><td>filter: none;</td></tr>
  </tbody>
</table>

### Deprecation notice

It is not required to use the `filter` class to use filters, this class
only exists for compatibility reasons.
