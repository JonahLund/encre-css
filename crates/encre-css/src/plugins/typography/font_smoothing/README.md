Utilities for controlling the font smoothing of an element.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>antialiased</td><td>-webkit-font-smoothing: antialiased;<br>-moz-osx-font-smoothing: grayscale;</td></tr>
    <tr><td>subpixel-antialiased</td><td>-webkit-font-smoothing: auto;<br>-moz-osx-font-smoothing: auto;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/font-smoothing)
