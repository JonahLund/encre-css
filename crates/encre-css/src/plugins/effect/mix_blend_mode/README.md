Utilities for controlling how an element should blend with the background.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>mix-blend-normal</td><td>mix-blend-mode: normal;</td></tr>
    <tr><td>mix-blend-multiply</td><td>mix-blend-mode: multiply;</td></tr>
    <tr><td>mix-blend-screen</td><td>mix-blend-mode: screen;</td></tr>
    <tr><td>mix-blend-overlay</td><td>mix-blend-mode: overlay;</td></tr>
    <tr><td>mix-blend-darken</td><td>mix-blend-mode: darken;</td></tr>
    <tr><td>mix-blend-lighten</td><td>mix-blend-mode: lighten;</td></tr>
    <tr><td>mix-blend-color-dodge</td><td>mix-blend-mode: color-dodge;</td></tr>
    <tr><td>mix-blend-color-burn</td><td>mix-blend-mode: color-burn;</td></tr>
    <tr><td>mix-blend-hard-light</td><td>mix-blend-mode: hard-light;</td></tr>
    <tr><td>mix-blend-soft-light</td><td>mix-blend-mode: soft-light;</td></tr>
    <tr><td>mix-blend-difference</td><td>mix-blend-mode: difference;</td></tr>
    <tr><td>mix-blend-exclusion</td><td>mix-blend-mode: exclusion;</td></tr>
    <tr><td>mix-blend-hue</td><td>mix-blend-mode: hue;</td></tr>
    <tr><td>mix-blend-saturation</td><td>mix-blend-mode: saturation;</td></tr>
    <tr><td>mix-blend-color</td><td>mix-blend-mode: color;</td></tr>
    <tr><td>mix-blend-luminosity</td><td>mix-blend-mode: luminosity;</td></tr>
    <tr><td>mix-blend-plus-lighter</td><td>mix-blend-mode: plus-lighter;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/mix-blend-mode)
