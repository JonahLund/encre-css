Utilities for controlling how the browser behaves when reaching the boundary of a scrolling area.

<table style="display: table;">
  <thead>
    <tr>
      <th style="text-align: center;">Class</th>
      <th style="text-align: center;">Properties</th>
    </tr>
  </thead>
  <tbody>
    <tr><td>overscroll-auto</td><td>overscroll-behavior: auto;</td></tr>
    <tr><td>overscroll-contain</td><td>overscroll-behavior: contain;</td></tr>
    <tr><td>overscroll-none</td><td>overscroll-behavior: none;</td></tr>
    <tr><td>overscroll-y-auto</td><td>overscroll-behavior-y: auto;</td></tr>
    <tr><td>overscroll-y-contain</td><td>overscroll-behavior-y: contain;</td></tr>
    <tr><td>overscroll-y-none</td><td>overscroll-behavior-y: none;</td></tr>
    <tr><td>overscroll-x-auto</td><td>overscroll-behavior-x: auto;</td></tr>
    <tr><td>overscroll-x-contain</td><td>overscroll-behavior-x: contain;</td></tr>
    <tr><td>overscroll-x-none</td><td>overscroll-behavior-x: none;</td></tr>
  </tbody>
</table>

[Tailwind reference](https://tailwindcss.com/docs/overscroll-behavior)
